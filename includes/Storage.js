var Storage = function(options) {
  var options = options || {};
  // ���������� ��������� �������
  if (!options.root) log.add("���� � ��������� ������� �� �����, ������������ ������� �������", "info");
  this.root = options.root || ".\\";
  
  this.extensions = options.extensions || ["jpg"];
  this.extensions_regex = null;

  this.fund_prefix = options.fund_prefix || "";
  this.fund_number = parseInt(options.fund_number || "");
  this.fund_letter = options.fund_letter || "";

  this.inventory_number = parseInt(options.inventory_number || "");
  this.inventory_letter = options.inventory_letter || "";
  this.inventory_volume = options.inventory_volume || "";

  this.unit_number = parseInt(options.unit_number || "");
  this.unit_letter = options.unit_letter || "";

  this.full_fund = null;
  this.rel_path_to_fund = null;
  this.abs_path_to_fund = null;
  this.full_inventory = null;
  this.rel_path_to_inventory = null;
  this.abs_path_to_inventory = null;
  this.full_unit = null;
  this.rel_path_to_unit = null;
  this.abs_path_to_unit = null;
  this.is_correct_details = null;

  this.fso = new ActiveXObject("Scripting.FileSystemObject");
  this.fldr = null;
  this.files = null;

  // �������� ������������ ���������� �����
  this.check_details();
  // ������������� ������ ������ � ����
  this.calculate_paths();
  // ������� ����������
  this.set_dir();
};

// �������� ������������ ���������� ������ �����, �����
Storage.prototype.check_details = function(force) {
  // ������������� ��������, ���� ����������� �������������� ����������
  // ��� �������� ������� �� �����������
  if (force || this.is_correct_details === null)
    this.is_correct_details = this.fund_number && this.inventory_number;
  return this.is_correct_details;
};

// ������������� ���� � ���������� �����, ������ ����� �����
Storage.prototype.get_full_fund = Storage.prototype.get_rel_path_to_fund = function(force) {
  if (force || this.full_fund === null || this.rel_path_to_fund === null)
    this.full_fund = this.rel_path_to_fund = (this.fund_prefix ? this.fund_prefix + "-" : "") +
      this.fund_number + (this.fund_letter ? "_" + this.fund_letter : "")
  return this.full_fund;
};

// ���������� ���� � ���������� �����
Storage.prototype.get_abs_path_to_fund = function(force) {
  if (force || this.abs_path_to_fund === null)
    this.abs_path_to_fund = this.root + this.get_rel_path_to_fund();
  return this.abs_path_to_fund;
};

// ������ ����� �����
Storage.prototype.get_full_inventory = function(force) {
  if (force || this.full_inventory === null)
    this.full_inventory = this.inventory_number + (this.inventory_letter ? "_" + this.inventory_letter : "")
       + (this.inventory_volume ? "_�" + this.inventory_volume : "");
  return this.full_inventory;
};

// ������������� ���� � ��������� �����
Storage.prototype.get_rel_path_to_inventory = function(force) {
  if (force || this.rel_path_to_inventory === null)
    this.rel_path_to_inventory = this.get_rel_path_to_fund() + "\\" + this.get_full_fund() + "-" + this.get_full_inventory();
  return this.rel_path_to_inventory;
};

// ���������� ���� � ���������� �����
Storage.prototype.get_abs_path_to_inventory = function(force) {
  if (force || this.abs_path_to_inventory === null)
    this.abs_path_to_inventory = this.get_abs_path_to_fund() + "\\" + this.get_full_fund() + "-" + this.get_full_inventory();
  return this.abs_path_to_inventory;
};

// ������ ����� ����
Storage.prototype.get_full_unit = function(force) {
  if (force || this.full_unit === null)
    this.full_unit = this.unit_number + (this.unit_letter ? "_" + this.unit_letter : "");
  return this.full_unit;
};

// ������������� ���� � ��������� ����
Storage.prototype.get_rel_path_to_unit = function(force) {
  if (force || this.rel_path_to_unit === null)
    this.rel_path_to_unit = this.get_rel_path_to_inventory() + "\\" + this.get_full_fund() + "-" +
      this.get_full_inventory() + "-" + this.get_full_unit();
  return this.rel_path_to_unit;
};

// ���������� ���� � ���������� ����
Storage.prototype.get_abs_path_to_unit = function(force) {
  if (force || this.abs_path_to_unit === null)
    this.abs_path_to_unit = this.get_abs_path_to_inventory() + "\\" + this.get_full_fund() + "-" +
      this.get_full_inventory() + "-" + this.get_full_unit();
  return this.abs_path_to_unit;
};

Storage.prototype.calculate_paths = function(force) {
  force = force || false;
  if (this.is_correct_details) {
    if (this.unit_number) {
      this.get_rel_path_to_unit(force);
      this.get_abs_path_to_unit(force);
    } else {
      this.get_rel_path_to_inventory(force);
      this.get_abs_path_to_inventory(force);
    }
  } else {
    log.add("������� ������ ��� ���������, ����� �����, ����� ��� ����: �. " + this.get_full_fund() + 
      " ��. " + this.get_full_inventory() + " �. " + this.get_full_unit(), "error");
  }
  return this;
};

Storage.prototype.extensions_to_regex = function(force) {
  if (force || this.extensions_regex === null)
    this.extensions_regex = new RegExp("\\.(" + this.extensions.join("|") + ")$", "i");
  return this.extensions_regex;
};

Storage.prototype.set_dir = function() {
  if (this.unit_number) {
    log.add("������ ���������� " + this.get_abs_path_to_unit());
  } else {
    log.add("������ ���������� " + this.get_abs_path_to_inventory());
  }
  try {
    if (this.unit_number) {
      this.fldr = this.fso.GetFolder(this.get_abs_path_to_unit());
    } else {
      this.fldr = this.fso.GetFolder(this.get_abs_path_to_inventory());
    }
  } catch(e) {
    if (this.unit_number) {
      log.add("�. " + this.get_full_fund() + " ��. " + this.get_full_inventory() + 
        " �. " + this.get_full_unit() + " �� ����� ����������� �������", "info");
    } else {
      log.add("�. " + this.get_full_fund() + " ��. " + this.get_full_inventory() + " �� ����� ����������� �������", "info");
    }
    this.fldr = null;
  }
};

Storage.prototype.get_files = function() {
  this.files = null;
  if (this.fldr !== null) {
    this.files = new Array();
    var files_list = new Enumerator(this.fldr.Files);
    for (;!files_list.atEnd(); files_list.moveNext()) {
      file = files_list.item();
      if (file.Name.match(this.extensions_to_regex())) this.files[file.Name] = file.Path;
    }
  }
  return this.files;
};