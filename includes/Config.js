var Config = {
  db: {
    AIS: {
      server: "SERVERNAME",
      name: "DBNAME",
      user: "USERNAME",
      pass: "PASSWORD",
      archive_id: 2
    },
    AF5: {
      server: "SERVERNAME",
      name: "DBNAME",
      user: "USERNAME",
      pass: "PASSWORD"
    },
    max_reconnects: 0, // ������� ��� ���������� ������� ����������� � ��. 0 - ����� ������� �� ���������.
    reconnect_interval: 5, // � ��������,
    // ������������ ���������� �������, ������������� � �� �� ���
    part_size: 1000,
    funds_rubric: "�������� ������",
    ignore_inventory_volume: true // ���������� ����� � ������ � ����
  },
  log: {
    level: ["debug", "info", "error", "critical"],
    file: ".\\log\\af5-to-ais.log",
    unique_name: true
  },
  storage: {
    units_root: ["\\\\PATH\\TO\\STORAGE\\", "\\\\PATH2\\TO\\STORAGE\\"], // ���� �� ����� ����������
    inventories_root: ["\\\\PATH\\TO\\INVENTORIES\\i\\"], // ���� �� ����� ����������
    extensions: ["jpg", "png"]
  }
};
